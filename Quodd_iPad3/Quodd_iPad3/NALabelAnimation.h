//
//  NALabelAnimation.h
//  Quood_iPad
//
//  Created by Nishant on 13/06/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NALabelAnimation : UIView

- (id)initWithFrame:(CGRect)frame andTickerArray:(NSArray*)tickers;

@end
