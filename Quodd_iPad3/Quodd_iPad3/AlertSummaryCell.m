//
//  AlertSummaryCell.m
//  Quodd_iPad2
//
//  Created by Nishant on 15/07/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import "AlertSummaryCell.h"

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@implementation AlertSummaryCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)loadCellAtIndex:(NSInteger)index withDictionary:(NSDictionary*)dict selected:(BOOL)selected
{
    _checkBox.selected = selected;
    _checkBox.tag = index;
    _editDeleteButton.tag = index;
    
    _editDeleteButton.userInteractionEnabled = index!=NSNotFound;
    
    static NSArray *labels;
    
    if (!labels) {
        labels = [[NSArray alloc] initWithObjects:
                  @"date",
                  @"ticker",
                  @"component",
                  @"condition",
                  @"value",
                  @"name",
                  @"comments",
                  nil];
    }
    
    for (int i=0; i<labels.count; i++) {
        
        NSString *key = labels[i];
        NSString *value = dict[key];
        
        UIButton *label = [self valueForKey:key];
        
        if (value) {
            
            if ([key isEqualToString:@"newsId"]) {
                value = [NSString stringWithFormat:@"%@", value];
                value = (value.boolValue || [value isEqualToString:@"N"]) ? @"N" : @"";
            }
            else if ([key isEqualToString:@"value"]) {
                value = [value stringByReplacingOccurrencesOfString:@"," withString:@" "];
            }

            [label setTitle:value forState:UIControlStateNormal];
        }
        else {
            [label setTitle:@"" forState:UIControlStateNormal];
        }
    }
}

- (IBAction)editDeleteBtn:(UIButton*)senderButton
{
    if (_delegate != nil && [_delegate respondsToSelector:@selector(editDeleteAlertAtIndex:)])
    {
        [_delegate editDeleteAlertAtIndex:senderButton.tag];
    }
}

- (IBAction)checkBoxPressed:(UIButton*)senderButton
{
    senderButton.selected = !senderButton.selected;
    
    if (_delegate != nil && [_delegate respondsToSelector:@selector(checkBoxAtIndex:)])
    {
        [_delegate checkBoxAtIndex:senderButton.tag];
    }
}

- (IBAction)showDetailAlert:(UIButton*)senderButton
{
    if (senderButton.titleLabel.text.length>0)
    {
        [[[UIAlertView alloc] initWithTitle:senderButton.titleLabel.text
                                    message:nil
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    }
}

@end
