//
//  MasterViewController.h
//  Quodd_iPad
//
//  Created by Nishant Anthwal on 2/20/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsDetailWebViewController.h"

#import "MajorIndicatorsIndicesVC.h"
#import "CompanyFundamentalsViewController.h"
#import "MiscViewController.h"
#import "TickerLookupViewController.h"
#import "OptionsPlusViewController.h"

#import "TickerTableView.h"
#import "AVFoundation/AVAudioPlayer.h"

#import "DetailQuoteView.h"

#import <MessageUI/MessageUI.h>

@interface MasterViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UIPopoverControllerDelegate, UITextFieldDelegate, UISearchBarDelegate, UIScrollViewDelegate, UIActionSheetDelegate, UIGestureRecognizerDelegate, TickerTableViewDelegate, DetailQuoteViewDelegate, AVAudioPlayerDelegate, MFMailComposeViewControllerDelegate>
{    
    IBOutlet UISearchBar *tickerLookupSearch;
    
    IBOutlet UIScrollView *greatWallScroll;
    
    IBOutlet UITableView *tickerTableView;
    
    NSString *lastTickerSelected;
    

    IBOutlet UITableView *newsTableView;
    
    UIPopoverController *popoverController;
    NSArray *popoverMenuItems;
    
    NSTimer *timerSnapQuote;
    NSTimer *timerComstockNewsList;

    NSDictionary *allMenuItems;
    NSDictionary *selectorForString;
    
    NSArray *quotesMenuItems;
    NSArray *newsMenuItems;
    NSMutableArray *companyInfoMenuItems;
    NSMutableArray *miscMenuItems;
    
    IBOutlet UILabel *tickerNameLabel;
    
    IBOutlet UILabel *lastKeyLabel;
    IBOutlet UILabel *lastValue;
    IBOutlet UILabel *openValue;
    IBOutlet UILabel *perChgValue;
    
    IBOutlet UILabel *chgValue;
    IBOutlet UILabel *highValue;
    
    IBOutlet UILabel *bidKeyLabel;
    IBOutlet UILabel *bidValue;
    IBOutlet UILabel *lowValue;
    
    IBOutlet UILabel *askKeyLabel;
    IBOutlet UILabel *askValue;
    IBOutlet UILabel *vwapValue;
    
    IBOutlet UILabel *volValue;
    IBOutlet UILabel *ltvValue;
    IBOutlet UILabel *lttValue;
    
    IBOutlet UILabel *calc1;
    IBOutlet UILabel *calc1Value;
    IBOutlet UILabel *calc2;
    IBOutlet UILabel *calc2Value;
    IBOutlet UILabel *calc3;
    IBOutlet UILabel *calc3Value;
    
    
    NSMutableArray *comstockNewsList;
    
    OptionsPlusViewController *optionsPlusVC;
    NewsDetailWebViewController *newsDetailVC;
    CompanyFundamentalsViewController *compFundaVC;
    MiscViewController *miscVC;
    TickerLookupViewController *tickerLookupVC;
    MajorIndicatorsIndicesVC *majorVC;
        
    IBOutlet UIScrollView *bottomNewsScroll;
}


- (IBAction)backgroundTouched;
- (IBAction)logoutButton;
- (IBAction)toggleEditingState:(UIBarButtonItem*)sender;

- (IBAction)showPopover:(UIButton*)senderButton;
- (IBAction)reloadGreatWallData;

- (IBAction)showHideMarketMonitorMenu:(BOOL)flag;
- (IBAction)greatWallSelected:(UISegmentedControl*)senderSegment;

- (IBAction)showTickerLookupView;
- (IBAction)showAlertVC;
- (IBAction)showSettingsVC;


@end
