//
//  DetailQuoteView.m
//  Quood_iPad
//
//  Created by Nishant on 19/06/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import "DetailQuoteView.h"

@interface DetailQuoteView ()

@property (nonatomic) TickerType type;
@property (nonatomic, strong) NSDictionary *formattedKeys;
@property (nonatomic, strong) NSMutableDictionary *dataDict;
@property (nonatomic, strong) NSMutableDictionary *completeDQdata;

@property (nonatomic, strong) NSArray *keyLabelsArray;
@property (nonatomic, strong) NSArray *valLabelsArray;
@property (nonatomic, strong) NSArray *keysToUse;

@property (nonatomic, strong) NSString *oldL;
@property (nonatomic, strong) NSString *oldL1;

@property (nonatomic, retain) IBOutlet UILabel *tickerNameLabel;
@property (nonatomic, retain) IBOutlet UILabel *isHalt;
@property (nonatomic, retain) IBOutlet UILabel *isSHO;


@property (nonatomic, retain) IBOutlet UILabel *keyLabel1;
@property (nonatomic, retain) IBOutlet UILabel *keyLabel2;
@property (nonatomic, retain) IBOutlet UILabel *keyLabel3;
@property (nonatomic, retain) IBOutlet UILabel *keyLabel4;
@property (nonatomic, retain) IBOutlet UILabel *keyLabel5;
@property (nonatomic, retain) IBOutlet UILabel *keyLabel6;
@property (nonatomic, retain) IBOutlet UILabel *keyLabel7;
@property (nonatomic, retain) IBOutlet UILabel *keyLabel8;
@property (nonatomic, retain) IBOutlet UILabel *keyLabel9;
@property (nonatomic, retain) IBOutlet UILabel *keyLabel10;
@property (nonatomic, retain) IBOutlet UILabel *keyLabel11;
@property (nonatomic, retain) IBOutlet UILabel *keyLabel12;
@property (nonatomic, retain) IBOutlet UILabel *keyLabel13;
@property (nonatomic, retain) IBOutlet UILabel *keyLabel14;
@property (nonatomic, retain) IBOutlet UILabel *keyLabel15;
@property (nonatomic, retain) IBOutlet UILabel *keyLabel16;
@property (nonatomic, retain) IBOutlet UILabel *keyLabel17;
@property (nonatomic, retain) IBOutlet UILabel *keyLabel18;
@property (nonatomic, retain) IBOutlet UILabel *keyLabel19;
@property (nonatomic, retain) IBOutlet UILabel *keyLabel20;
@property (nonatomic, retain) IBOutlet UILabel *keyLabel21;
@property (nonatomic, retain) IBOutlet UILabel *keyLabel22;
@property (nonatomic, retain) IBOutlet UILabel *keyLabel23;
@property (nonatomic, retain) IBOutlet UILabel *keyLabel24;
@property (nonatomic, retain) IBOutlet UILabel *keyLabel25;
@property (nonatomic, retain) IBOutlet UILabel *keyLabel26;
@property (nonatomic, retain) IBOutlet UILabel *keyLabel27;
@property (nonatomic, retain) IBOutlet UILabel *keyLabel28;
@property (nonatomic, retain) IBOutlet UILabel *keyLabel29;
@property (nonatomic, retain) IBOutlet UILabel *keyLabel30;



@property (nonatomic, retain) IBOutlet UILabel *valLabel1;
@property (nonatomic, retain) IBOutlet UILabel *valLabel2;
@property (nonatomic, retain) IBOutlet UILabel *valLabel3;
@property (nonatomic, retain) IBOutlet UILabel *valLabel4;
@property (nonatomic, retain) IBOutlet UILabel *valLabel5;
@property (nonatomic, retain) IBOutlet UILabel *valLabel6;
@property (nonatomic, retain) IBOutlet UILabel *valLabel7;
@property (nonatomic, retain) IBOutlet UILabel *valLabel8;
@property (nonatomic, retain) IBOutlet UILabel *valLabel9;
@property (nonatomic, retain) IBOutlet UILabel *valLabel10;
@property (nonatomic, retain) IBOutlet UILabel *valLabel11;
@property (nonatomic, retain) IBOutlet UILabel *valLabel12;
@property (nonatomic, retain) IBOutlet UILabel *valLabel13;
@property (nonatomic, retain) IBOutlet UILabel *valLabel14;
@property (nonatomic, retain) IBOutlet UILabel *valLabel15;
@property (nonatomic, retain) IBOutlet UILabel *valLabel16;
@property (nonatomic, retain) IBOutlet UILabel *valLabel17;
@property (nonatomic, retain) IBOutlet UILabel *valLabel18;
@property (nonatomic, retain) IBOutlet UILabel *valLabel19;
@property (nonatomic, retain) IBOutlet UILabel *valLabel20;
@property (nonatomic, retain) IBOutlet UILabel *valLabel21;
@property (nonatomic, retain) IBOutlet UILabel *valLabel22;
@property (nonatomic, retain) IBOutlet UILabel *valLabel23;
@property (nonatomic, retain) IBOutlet UILabel *valLabel24;
@property (nonatomic, retain) IBOutlet UILabel *valLabel25;
@property (nonatomic, retain) IBOutlet UILabel *valLabel26;
@property (nonatomic, retain) IBOutlet UILabel *valLabel27;
@property (nonatomic, retain) IBOutlet UILabel *valLabel28;
@property (nonatomic, retain) IBOutlet UILabel *valLabel29;
@property (nonatomic, retain) IBOutlet UILabel *valLabel30;


@end

@implementation DetailQuoteView

- (void)configureDetailQuoteViewForTicker:(Ticker*)ticker
{
    [_detailIndicator startAnimating];
    
    _ticker = ticker;
    
    //set ticker type
    NSInteger tickerLength = ticker.tickerNameByUser.length;
    BOOL isMontage = (tickerLength > 3) && ([ticker.tickerNameByUser characterAtIndex:tickerLength-2] == '/');
    BOOL isOptions = ([ticker.tickerNameByUser hasPrefix:@"O:"] || [ticker.tickerNameByUser rangeOfString:@" "].location != NSNotFound) && !isMontage;
    BOOL isBond = [ticker.tickerNameByUser hasPrefix:@"B:"];
    
    NSDictionary *futures = DICT_FUTURES;
    NSArray *futuresKeys = futures.allKeys;
    NSArray *futuresValues = futures.allValues;
    
    BOOL isFutures = [ticker.ticker hasPrefix:@"/"] || [futuresKeys containsObject:ticker.tickerNameByUser] || [futuresValues containsObject:ticker.tickerNameByUser];
    
    
    if (isFutures) {
        _type = FUTURES;
    } else if (isOptions) {
        _type = OPTIONS;
    } else if (isBond) {
        _type = BONDS;
    } else _type = EQUITY;

    
    [self initArrays];

    _tickerNameLabel.text = ticker.tickerNameByUser;
    _isHalt.hidden = YES;
    _isSHO.hidden = YES;
    
    for (int i=0; i<_keyLabelsArray.count; i++)
    {
        UILabel *keyLabel = _keyLabelsArray[i];
        keyLabel.text = @"";
        
        UILabel *valLabel = _valLabelsArray[i];
        valLabel.text = @"";
    }

    for (int i=0; i<_keysToUse.count; i++)
    {
        NSString *key = _keysToUse[i];

        UILabel *keyLabel = _keyLabelsArray[i];
        keyLabel.text = _formattedKeys[key] ? _formattedKeys[key] : key;
        [keyLabel sizeToFit];

        UILabel *valLabel = _valLabelsArray[i];
        valLabel.text = @"";
    }

    if (ticker.subscription == UNSUBSCRIBED) {
        [_detailIndicator stopAnimating];
        return;
    }

    [self performSelectorInBackground:@selector(getDataForSymbol:) withObject:ticker.tickerNameByUser];
}

- (void)initArrays
{
    _keyLabelsArray = [[NSArray alloc] initWithObjects:
                       _keyLabel1, _keyLabel2, _keyLabel3, _keyLabel4, _keyLabel5,
                       _keyLabel6, _keyLabel7, _keyLabel8, _keyLabel9, _keyLabel10,
                       _keyLabel11, _keyLabel12, _keyLabel13, _keyLabel14, _keyLabel15,
                       _keyLabel16, _keyLabel17, _keyLabel18, _keyLabel19, _keyLabel20,
                       _keyLabel21, _keyLabel22, _keyLabel23, _keyLabel24, _keyLabel25,
                       _keyLabel26, _keyLabel27, _keyLabel28, _keyLabel29, _keyLabel30,
                       nil];
    
    
    _valLabelsArray = [[NSArray alloc] initWithObjects:
                       _valLabel1, _valLabel2, _valLabel3, _valLabel4, _valLabel5,
                       _valLabel6, _valLabel7, _valLabel8, _valLabel9, _valLabel10,
                       _valLabel11, _valLabel12, _valLabel13, _valLabel14, _valLabel15,
                       _valLabel16, _valLabel17, _valLabel18, _valLabel19, _valLabel20,
                       _valLabel21, _valLabel22, _valLabel23, _valLabel24, _valLabel25,
                       _valLabel26, _valLabel27, _valLabel28, _valLabel29, _valLabel30,
                       nil];
    
    
    NSArray *equityKeys = [[NSArray alloc] initWithObjects:             //order is important
                           @"last", @"chg", @"bid", @"ask", @"s",
                           @"open", @"high", @"low", @"Div", @"vol",
                           @"lastClosed", @"YrHi", @"YrLo", @"Yield", @"ltv",
                           @"perChg", @"MCap", @"PE", @"ExDiv", @"ltt",
                           @"exchg", @"CUSIP", @"EPS", @"vwap", @"AV10DAY",
                           @"Sector", @"Industry", @"nd", @"P/P", @"ShOut",
                           nil];
        
    NSArray *futureKeys = [[NSArray alloc] initWithObjects: //order is important
                           @"last", @"chg", @"bid", @"ask", @"s",
                           @"vol", @"L1", @"high", @"low", @"CH",
                           @"CL", @"L2", @"OpI", @"settlementPrice", @"ltv",
                           @"Pst", @"LTD", @"exchg", @"ORange",
                           nil];
    
    NSArray *bondsKeys = [[NSArray alloc] initWithObjects: //order is important
                          @"last", @"chg", @"bid", @"ask", @"s",
                          @"vol", @"CUSIP", @"IntRate", @"FaceVlu", @"MatDate",
                          @"exchg",
                          nil];
    
    
    NSArray *optionsKeys = [[NSArray alloc] initWithObjects: //order is important
                            @"last", @"chg", @"bid", @"ask", @"s",
                            @"vol", @"open", @"high", @"low", @"CH",
                            @"CL", @"ltv", @"lastClosed", @"perChg", @"OpI",
                            @"Strike", @"Exp.", @"ltt",
                            nil];
    
    
    NSDictionary *equityLabels = [[NSDictionary alloc] initWithObjectsAndKeys:
                                  @"Ask", @"ask",
                                  @"Ask Exchg", @"askExchg",
                                  @"Ask Size", @"askSize",
                                  @"Bid", @"bid",
                                  @"Bid Exchg", @"bidExchg",
                                  @"Bid Size", @"bidSize",
                                  @"S", @"s",
                                  @"Chg", @"chg",
                                  @"Exchg", @"exchg",
                                  @"High", @"high",
                                  @"Last", @"last",
                                  @"Low", @"low",
                                  @"LTT", @"ltt",
                                  @"LTV", @"ltv",
                                  @"Open", @"open",
                                  @"%Chg", @"perChg",
                                  @"PC", @"lastClosed",
                                  @"Settlement Price", @"settlementPrice",
                                  @"Ticker", @"ticker",
                                  @"Ticker", @"tickerNameByUser",
                                  @"Vol", @"vol",
                                  @"VWAP", @"vwap",
                                  @"News", @"nd",
                                  nil];
    
    NSDictionary *futureLabels = [[NSDictionary alloc] initWithObjectsAndKeys:
                                  @"L", @"last",
                                  @"Chg", @"chg",
                                  @"B", @"bid",
                                  @"A", @"ask",
                                  @"QT Size", @"s",
                                  
                                  @"Vlm", @"vol",
                                  @"L1", @"L1",
                                  @"Hi", @"high",
                                  @"Lo", @"low",
                                  @"CH", @"CH",
                                  
                                  @"CL", @"CL",
                                  @"Vlm-f", @"Vlm-f",
                                  @"L2", @"L2",
                                  @"OpI", @"OpI",
                                  @"Stl", @"settlementPrice",
                                  
                                  @"Tvl", @"ltv",
                                  @"Pst", @"Pst",
                                  @"LTD", @"LTD",
                                  @"Exchg", @"exchg",
                                  @"ORange", @"oRange", nil];
    
    NSDictionary *bondsLabels = [[NSDictionary alloc] initWithObjectsAndKeys:
                                 @"Last", @"last",
                                 @"Chg", @"chg",
                                 @"Bid", @"bid",
                                 @"Ask", @"ask",
                                 @"S", @"s",
                                 
                                 @"Vol", @"vol",
                                 @"CUSIP", @"CUSIP",
                                 @"IntRate", @"IntRate",
                                 @"FaceVlu", @"FaceVlu",
                                 @"MatDate", @"MatDate",
                                 
                                 @"Exchg", @"exchg", nil];
    
    NSDictionary *optionsLabels = [[NSDictionary alloc] initWithObjectsAndKeys:
                                   @"Last", @"last",
                                   @"Chg", @"chg",
                                   @"Bid", @"bid",
                                   @"Ask", @"ask",
                                   @"S", @"s",
                                   
                                   @"Vol", @"vol",
                                   @"Open", @"open",
                                   @"High", @"high",
                                   @"Low", @"low",
                                   @"CH", @"CH",

                                   @"CL", @"CL",
                                   @"LTV", @"ltv",
                                   @"PC", @"lastClosed",
                                   @"%Chg", @"perChg",
                                   @"OpI", @"OpI",
                                   
                                   @"Strike", @"Strike",
                                   @"Exp.", @"Exp.",
                                   @"LTT", @"ltt", nil];
    
    switch (_type)
    {
        case FUTURES:
            _keysToUse = [[NSArray alloc] initWithArray:futureKeys];
            _formattedKeys = [[NSDictionary alloc] initWithDictionary:futureLabels];
            break;
            
        case BONDS:
            _keysToUse = [[NSArray alloc] initWithArray:bondsKeys];
            _formattedKeys = [[NSDictionary alloc] initWithDictionary:bondsLabels];
            break;
            
        case OPTIONS:
            _keysToUse = [[NSArray alloc] initWithArray:optionsKeys];
            _formattedKeys = [[NSDictionary alloc] initWithDictionary:optionsLabels];
            break;
            
        default:
            _keysToUse = [[NSArray alloc] initWithArray:equityKeys];
            _formattedKeys = [[NSDictionary alloc] initWithDictionary:equityLabels];
            break;
    }
}

- (void)getDataForSymbol:(NSString*)tickerSymbol
{
    if (!_dataDict) {
        _dataDict = [[NSMutableDictionary alloc] init];
    }
    else [_dataDict removeAllObjects];
    
    BOOL isFuture = [_ticker.ticker hasPrefix:@"/"];
    NSString *tempSymbol = isFuture ? _ticker.ticker : tickerSymbol;
    
    if ([_ticker.ticker hasPrefix:@"O:"]) {
        tempSymbol = _ticker.ticker;
    }
    
    //collect snap data from ticker
    NSDictionary *dict = [_ticker dictionaryWithValuesForKeys:_keysToUse];
    [_dataDict addEntriesFromDictionary:dict];
    
    if (!_completeDQdata) {
        _completeDQdata = [[NSMutableDictionary alloc] init];
    }
    
    if ([_completeDQdata.allKeys containsObject:tempSymbol]) {
        [_dataDict addEntriesFromDictionary:_completeDQdata[tempSymbol]];
    }
    else if (tempSymbol) {
        
        //get detail data for ticker
        NSString *detailUrl = [NSString stringWithFormat:DETAILED_QUOTES_URL, tempSymbol];
        NSDictionary *detailDict = [NSDictionary dictionaryWithDictionary:[ProjectHandler dataFromAPIWithStringURL:detailUrl]];
        [_dataDict addEntriesFromDictionary:detailDict[tempSymbol]];
        
        [_completeDQdata setValue:detailDict[tempSymbol] forKey:tempSymbol];
    }
    
    //replace NULL values
    for (int i=0; i<_keysToUse.count; i++) {
        if ([_dataDict[_keysToUse[i]] isKindOfClass:[NSNull class]]) {
            [_dataDict setValue:@" " forKey:_keysToUse[i]];
        }
    }
    
    if (_ticker.last) //check if snap data is available //if YES - calculate the other values
    {
        NSNumberFormatter *numberFormatter=[[NSNumberFormatter alloc] init];
        [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
        [numberFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
        [numberFormatter setCurrencySymbol:@""];
        [numberFormatter setMaximumFractionDigits:2];

        
        CGFloat lastFloat  = [_dataDict[@"last"] floatValue];
        CGFloat shOutFloat = [_dataDict[@"ShOut"] floatValue];
        CGFloat epsFloat   = [_dataDict[@"EPS"] floatValue];
        CGFloat divFloat   = [_dataDict[@"Div"] floatValue];
        
        NSNumber *MCap = [NSNumber numberWithFloat: lastFloat*shOutFloat];
        
        NSString *key = @"MCap";
        NSString *val = [numberFormatter stringFromNumber:MCap];
        [_dataDict setValue:val forKey:key];
        
        key = @"PE";
        val = [NSString stringWithFormat:@"%0.2f",lastFloat/epsFloat];
        [_dataDict setValue:epsFloat ? val : @"N/A" forKey:key];
        
        key = @"Yield";
        val = [NSString stringWithFormat:@"%0.2f",(divFloat/lastFloat)*100];
        [_dataDict setValue:val forKey:key];
        
        if (_type == FUTURES)
        {
            key = @"Pst";
            val = [_dataDict[key] floatValue]>0 ? _dataDict[key] : @"";
            [_dataDict setValue:val forKey:key];
            
            key = @"ORange";
            if ([_dataDict[@"oRange"] floatValue] > 0) {
                val = _dataDict[@"oRange"];
            }
            else val = _dataDict[key];
            
            [_dataDict setValue:val forKey:key];
        }
        
        if ([Volume_Tickers containsObject:self.ticker.ticker])
        {
            key = @"last";
            val = [self millionFormatForValue:_dataDict[key]];
            [_dataDict setValue:val forKey:key];
        }
    }
    
    if (_dataDict.count)
        [self performSelectorOnMainThread:@selector(setViewForTicker:) withObject:tickerSymbol waitUntilDone:YES];

}

- (void)setViewForTicker:(NSString*)tickerSymbol
{
    @try
    {
        _ticker.tickerNameByUser = _ticker.tickerNameByUser ? _ticker.tickerNameByUser : @"";
        tickerSymbol = tickerSymbol ? tickerSymbol : @"";
        
        BOOL isFuture = [_ticker.ticker hasPrefix:@"/"];
        if (_dataDict[@"companyName"])
            _tickerNameLabel.text = [NSString stringWithFormat:@"%@ - %@", isFuture ? _ticker.tickerNameByUser : tickerSymbol, _dataDict[@"companyName"]];
        else _tickerNameLabel.text = @"";
        
        _isHalt.hidden = _dataDict[@"isHalt"] ? [_dataDict[@"isHalt"] isEqualToString:@"F"] : YES;
        _isSHO.hidden = _dataDict[@"isSHO"] ? [_dataDict[@"isSHO"] isEqualToString:@"F"] : YES;
        
        for (int i=0; i<_keyLabelsArray.count; i++)
        {
            UILabel *keyLabel = _keyLabelsArray[i];
            
            if ([keyLabel.text isEqualToString:@"Last"] && _ticker.marketCenter && _ticker.marketCenter.length<3) {
                keyLabel.text = [NSString stringWithFormat:@"Last %@", _ticker.marketCenter];
            }
            else if ([keyLabel.text isEqualToString:@"Ask"] && _ticker.askExchg && _ticker.askExchg.length<3) {
                keyLabel.text = [NSString stringWithFormat:@"Ask %@", _ticker.askExchg];
            }
            else if ([keyLabel.text isEqualToString:@"Bid"] && _ticker.bidExchg && _ticker.bidExchg.length<3) {
                keyLabel.text = [NSString stringWithFormat:@"Bid %@", _ticker.bidExchg];
            }
            
            [keyLabel sizeToFit];
        }
        
        NSInteger i=0;
        for (i=0; i<_keysToUse.count; i++)
        {
            NSString *key = _keysToUse[i];
            
            UILabel *valLabel = _valLabelsArray[i];
            valLabel.textColor = [UIColor cyanColor];

            valLabel.text = _dataDict[key] ? _dataDict[key] : @"";
            
            if ([key isEqualToString:@"chg"] || [key isEqualToString:@"perChg"])
            {
                if (valLabel.text.floatValue>0)
                {
                    valLabel.textColor = [UIColor greenColor];
                }
                else if (valLabel.text.floatValue<0)
                {
                    valLabel.textColor = [UIColor redColor];
                }
            }
            else if ([key isEqualToString:@"high"] && valLabel.text.floatValue!=0.0)
            {
                UIColor *highColor = (valLabel.text.floatValue > [_dataDict[@"YrHi"] floatValue]) ? [UIColor greenColor] : [UIColor cyanColor];
                valLabel.textColor = highColor;
            }
            else if ([key isEqualToString:@"low"] && valLabel.text.floatValue!=0.0)
            {
                UIColor *lowColor = (valLabel.text.floatValue < [_dataDict[@"YrLo"] floatValue]) ? [UIColor redColor] : [UIColor cyanColor];
                valLabel.textColor = lowColor;
            }
        }
        
        while (i<_keyLabelsArray.count)
        {
            [_keyLabelsArray[i] setText:@""];
            [_valLabelsArray[i] setText:@""];
            
            i++;
        }
    }
    @catch (NSException *exception)
    {
        //NSLog(@"%s - %@", __PRETTY_FUNCTION__, exception.description);
    }
    
    [_detailIndicator stopAnimating];
}

- (NSString*)millionFormatForValue:(NSString*)floatString
{
    if ([floatString hasSuffix:@" M"]) {
        return floatString;
    }
    
    NSInteger num = floatString.integerValue;
    
    NSInteger millionInt = num/1000000;
    NSInteger millionDecimal = num%1000000;
    
    floatString = [NSString stringWithFormat:@"%d.%d", millionInt, millionDecimal];

    NSNumberFormatter *numberFormatter=[[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [numberFormatter setCurrencySymbol:@""];
    numberFormatter.maximumFractionDigits = 2;

    NSNumber *formattedFloat = [NSNumber numberWithFloat: floatString.floatValue];
    
    NSString *milNum = [NSString stringWithFormat:@"%@ M",[numberFormatter stringFromNumber:formattedFloat]];
    
    return milNum;
}

#pragma mark - IBActions
- (IBAction)showSector
{
    if (_valLabel26.text.length>0) {
        
        [[[UIAlertView alloc] initWithTitle:@"Sector"
                                    message:_valLabel26.text
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
        
    }
}

- (IBAction)showIndustry
{
    if (_valLabel27.text.length>0) {
        
        [[[UIAlertView alloc] initWithTitle:@"Industry"
                                    message:_valLabel27.text
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
        
    }
}

- (IBAction)showNews
{
    if (_valLabel28.text.length>2) {
        
        if (_delegate != nil && [_delegate respondsToSelector:@selector(loadCompanyNewsForTicker:)])
        {
            [_delegate loadCompanyNewsForTicker:_ticker.ticker];
        }
    }
}

#pragma mark - New Ticker
- (IBAction)updateTicker
{
    NSString *oldTicker = _ticker.tickerNameByUser;
    
    UIAlertView *newTickerAlert = [[UIAlertView alloc] initWithTitle:@"New Ticker for Detail Quote"
                                                             message:[NSString stringWithFormat:@"Enter new name for '%@'",oldTicker]
                                                            delegate:self
                                                   cancelButtonTitle:@"Cancel"
                                                   otherButtonTitles:@"OK", nil];
    
    newTickerAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
    
    [[newTickerAlert textFieldAtIndex:0] setText:oldTicker];
    [[newTickerAlert textFieldAtIndex:0] setTextAlignment:NSTextAlignmentCenter];
    
    [newTickerAlert show];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==1)
    {
        NSString *newTicker = [[alertView textFieldAtIndex:0] text];
        
        Ticker *newTickerOb = [[Ticker alloc] init];
        newTickerOb.tickerNameByUser = newTicker;
        
        //check ticker updater
        NSString *tickerUpdaterURL = [NSString stringWithFormat:TICKER_UPDATER_URL, newTicker, currentUserName];
        NSMutableArray *updatedTicker = [ProjectHandler dataFromAPIWithStringURL:tickerUpdaterURL];
        
        newTicker = updatedTicker.count>0 ? updatedTicker.lastObject : newTicker;
        
        newTickerOb.ticker = newTicker;
        
        
        NSDictionary *dict = [ProjectHandler dataFromAPIWithStringURL:newTicker];
        [newTickerOb loadWithTicker:dict forSymbol:newTicker];
        
        [self configureDetailQuoteViewForTicker:newTickerOb];
    }
}

@end
