//
//  DetailQuoteView.h
//  Quood_iPad
//
//  Created by Nishant on 19/06/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Ticker.h"

@protocol DetailQuoteViewDelegate <NSObject>

- (void)loadCompanyNewsForTicker:(NSString *)symbol;

@end

@interface DetailQuoteView : UIView<UIAlertViewDelegate>

@property (unsafe_unretained, nonatomic) id<DetailQuoteViewDelegate> delegate;

@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *detailIndicator;
@property (nonatomic, retain) Ticker *ticker;


- (void)configureDetailQuoteViewForTicker:(Ticker*)ticker;
- (IBAction)showSector;
- (IBAction)showIndustry;
- (IBAction)showNews;


@end
