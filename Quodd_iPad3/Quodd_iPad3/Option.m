//
//  Option.m
//  Quodd_iPad2
//
//  Created by Paxcel Technologies on 15/07/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import "Option.h"

@interface Option ()

@property (strong, nonatomic) NSArray *keyArray;

@end


@implementation Option

- (id) initWithDictionary:(NSDictionary *) dictionary
               volatility: (NSString *) volatility
{
    self = [super init];
    if (self)
    {
        _volatility = volatility.doubleValue;

        _expireTime = [dictionary[@"expireTime"] doubleValue];
        _keyArray = dictionary[@"keys"];
        _interestRate = [dictionary[@"interestRate"] doubleValue];

        _optionItemArray = [self tickerArrayForArray:dictionary[@"optionItem"]];
    }

    return self;
}

//return array of ticker objects
- (NSArray *) tickerArrayForArray: (NSArray *) array
{
    NSMutableArray *tempArray = [[NSMutableArray alloc] initWithCapacity:array.count];
    Ticker *ticker;
    
    for (NSArray *innerArray in array)
    {
        ticker = [[Ticker alloc] initWithOptionArray:innerArray
                                      parseKeyFormat:_keyArray];
        ticker.volatility = _volatility;
        ticker.rate = _interestRate;
        [tempArray addObject:ticker];
    }

    return tempArray;
}

//refresh data after getting new data.
- (void) updateOptionItemArrayWithDictionary: (NSDictionary *) dictionary
                                  rootTicker: (Ticker *) rootTicker
{
    for (Ticker *ticker in _optionItemArray)
    {
        if (dictionary[ticker.ticker])
        {
            // modification for mini tickers
            NSString *str = [ticker.ticker componentsSeparatedByString:@"\\"][0];
            str = [str substringFromIndex:2];
            
            BOOL isMiniTicker = ![str isEqualToString:rootTicker.tickerNameByUser];
            
            if (isMiniTicker) {
                rootTicker.tickerNameByUser = str;
            }
            
            ticker.rootTicker = rootTicker;
            [ticker loadWithTicker:dictionary[ticker.ticker]
                         forSymbol:ticker.tickerNameByUser];
        }
    }
}

@end