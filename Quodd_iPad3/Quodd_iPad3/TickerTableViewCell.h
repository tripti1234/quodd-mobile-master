//
//  TickerTableViewCell.h
//  Quodd_iPad
//
//  Created by Paxcel Technologies on 16/03/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Ticker.h"

@protocol TickerTableViewCellDelegate <NSObject>

//- (void)companyNewsForTicker:(NSInteger)tickerIndex;
- (void)companyNewsForNewsID:(NSString*)newsID;

@end


@interface TickerTableViewCell : UITableViewCell

@property (unsafe_unretained, nonatomic) id<TickerTableViewCellDelegate> delegate;
@property (nonatomic) BOOL isRead;

- (id) initWithFrame:(CGRect)frame;// andTicker : (Ticker *) ticker;
- (void) loadCellWithTicker: (Ticker *) ticker andColumns:(NSArray*)col;

@end
