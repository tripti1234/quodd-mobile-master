//
//  TickerTableViewCell.m
//  Quodd_iPad
//
//  Created by Paxcel Technologies on 16/03/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import "TickerTableViewCell.h"
#import "HGMessageHUD.h"

#import <QuartzCore/QuartzCore.h>

@interface TickerTableViewCell ()

@property (strong, nonatomic) NSMutableArray *labelArray;
@property (strong, nonatomic) NSArray *columnArray;

@end

@implementation TickerTableViewCell

- (id) initWithFrame:(CGRect)frame// andTicker : (Ticker *) ticker
{
    self = [super initWithFrame:frame];
    if (self) {
        self.frame = frame;
    }
    return self;
}

- (void) loadCellWithTicker: (Ticker *) ticker andColumns:(NSArray*)col
{
    static NSDictionary *columnWidth;
    
    if (!columnWidth) {
        columnWidth = [[NSDictionary alloc] initWithDictionary:[(AppDelegate *)[[UIApplication sharedApplication] delegate] columnWidth]];
    }

    _columnArray = col;
    
    _labelArray = [[NSMutableArray alloc] initWithCapacity:_columnArray.count];
        
    UIColor *greenText = [UIColor colorWithRed:18.0/255.0 green:1.0 blue:0.0 alpha:1.0];
    
    CGFloat buffer = 10;
    CGFloat desiredTotalWidth = self.frame.size.width - 2*buffer;
    CGFloat actualTotalWidth = 0.0;
    
    for (NSString *col in _columnArray)
    {
        actualTotalWidth += [columnWidth[col] floatValue];
    }
    
    CGFloat multiplicationFactor = desiredTotalWidth / actualTotalWidth;
    
    CGFloat x = 0;
    CGFloat colHeight = self.frame.size.height;
    CGFloat colWidth;
    UILabel *label;
    
    for (int i = 0; i< _columnArray.count; i++)
    {
        colWidth = [columnWidth[_columnArray[i]] floatValue] * multiplicationFactor;
        
        label = [[UILabel alloc] initWithFrame:CGRectMake(x+buffer, 0, colWidth, colHeight)];
        label.textColor = [UIColor whiteColor];
        label.backgroundColor = [UIColor clearColor];
        //label.font = [UIFont fontWithName:@"Georgia" size:18.0];
        ////NSLog(@"%@", [UIFont fontNamesForFamilyName:@"Georgia"]);
        [self.contentView addSubview:label];

        //set text color using change value
        NSString *changeVal = [ticker valueForKey:@"chg"];
        if (changeVal.floatValue>0) {
            label.textColor = greenText;
        }
        else if (changeVal.floatValue<0) {
            label.textColor = [UIColor redColor];
        }

        if (ticker.subscription == UNSUBSCRIBED)
        {
            label.textColor = [UIColor whiteColor];
            if (i>0) break;
        }

        if ([ticker valueForKey:_columnArray[i]] && ![[ticker valueForKey:_columnArray[i]] isEqualToString:@"null"]) {
            //explicitly convert all values to string
            label.text = [NSString stringWithFormat:@"%@", [ticker valueForKey:_columnArray[i]]];
        }
        
        //check if News(N) column
        if ([_columnArray[i] isEqualToString:@"ni"])
        {
            label.text = [[ticker valueForKey:_columnArray[i]] boolValue] ? @"N" : @"";

            if ([[ticker valueForKey:_columnArray[i]] boolValue]) {
                
                label.textColor = _isRead ? [UIColor redColor] : [UIColor whiteColor];
                
                UIButton *newsButton = [UIButton buttonWithType:UIButtonTypeCustom];
                [newsButton setFrame:label.frame];
                [newsButton addTarget:self action:@selector(getNews:) forControlEvents:UIControlEventTouchUpInside];
                newsButton.tag = [[ticker valueForKey:_columnArray[i]] integerValue];
                [newsButton setTitle:ticker.ni forState:UIControlStateNormal];
                [newsButton setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];

                [self.contentView addSubview:newsButton];
            }
        }
        
        if ([_columnArray[i] isEqualToString:@"isHalt"])
        {
            label.text = [[ticker valueForKey:_columnArray[i]] isEqualToString:@"T"] ? @"H" : @"";
        }
        
        if ([@[@"T", @"A", @"ni", @"isHalt", @"marketCenter", @"askExchg", @"bidExchg", @"cpp"] containsObject:_columnArray[i]]) {
            label.textAlignment = NSTextAlignmentCenter;
        } else if (i>0) {
            label.textAlignment = NSTextAlignmentRight;
        }
        
        if (i == 0 && ticker.strike)
        {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = label.frame;
            
            [button addTarget:self
                       action:@selector(copyToPasteBoard:)
             forControlEvents:UIControlEventTouchUpInside];
            button.titleLabel.text = label.text;

            [self.contentView addSubview:button];
        }

        //[self.contentView addSubview:label]; //done on top
        [_labelArray addObject:label];
        x += colWidth;
    }
}

- (IBAction)getNews:(UIButton*)newsButton
{
    if (_delegate != nil && [_delegate respondsToSelector:@selector(companyNewsForNewsID:)])
    {
        [_delegate companyNewsForNewsID:newsButton.titleLabel.text];
    }
}

- (void)copyToPasteBoard:(UIButton*)senderButton
{
    if (senderButton.titleLabel.text.length>0)
    {
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string = senderButton.titleLabel.text;
        [HGMessageHUD showInView:self withMessage:@"Copied to Pasteboard"];
    }
}

//column reordering //not used
- (void) moveObjectAtIndex: (NSUInteger) fromIndex toIndex: (NSUInteger) toIndex
{
    id temp = _labelArray[fromIndex];
    CGRect finalFrame = [_labelArray[toIndex] frame];
    CGFloat labelWidth = finalFrame.size.width;
    NSUInteger i = fromIndex;
    if (fromIndex < toIndex)
        while (i< toIndex)
        {
            CGRect temp = [(UILabel *)_labelArray[i+1] frame];
            [(UILabel *)_labelArray[i+1] setFrame:CGRectMake(i * labelWidth, temp.origin.y, temp.size.width, temp.size.height)];
            _labelArray[i] = _labelArray[++i];
        }
    else
        while (i> toIndex)
        {
            CGRect temp = [(UILabel *)_labelArray[i-1] frame];
            [(UILabel *)_labelArray[i-1] setFrame:CGRectMake(i * labelWidth, temp.origin.y, temp.size.width, temp.size.height)];
            _labelArray[i] = _labelArray[--i];
        }
    [(UILabel *)temp setFrame:finalFrame];
    _labelArray[toIndex] = temp;
}

@end