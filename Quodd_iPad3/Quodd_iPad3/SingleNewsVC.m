//
//  SingleNewsVC.m
//  Quodd_iPad3
//
//  Created by Nishant on 02/08/13.
//  Copyright (c) 2013 Paxcel Technologies. All rights reserved.
//

#import "SingleNewsVC.h"

@interface SingleNewsVC ()

@end

@implementation SingleNewsVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSURL *newsURL = [NSURL URLWithString:_newsURL];
    [_webViewSingleNews loadRequest:[NSURLRequest requestWithURL:newsURL]];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return (toInterfaceOrientation==UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation==UIInterfaceOrientationLandscapeRight);
}

#pragma mark - IBActions
- (IBAction)close
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIWebViewDelegate
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [_newsIndicator startAnimating];
    webView.hidden = YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [_newsIndicator stopAnimating];
    
    float scale = 150.0;
    NSString *jsString = [[NSString alloc] initWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '%f%%'", scale];
    [webView stringByEvaluatingJavaScriptFromString:jsString];
    
    webView.hidden = NO;
}

@end
