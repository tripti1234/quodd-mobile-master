//
//  MarketMonitor.h
//  Quodd_iPad
//
//  Created by Nishant on 29/05/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MarketMonitor : NSObject

@property (nonatomic, strong) NSString *index;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSMutableArray *greatWalls;

@end
