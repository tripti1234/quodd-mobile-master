//
//  MajorIndicatorsIndicesVC.m
//  Quodd_iPad
//
//  Created by Nishant on 05/06/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import "MajorIndicatorsIndicesVC.h"

#define INDICATORS_TABLE_TAG    987
#define INDICES_TABLE_TAG       456

#define INDICES_TABLE_ROWHEIGHT 34.0

@interface MajorIndicatorsIndicesVC ()

@property (nonatomic, retain) IBOutlet UITableView *tableView;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *titleItem;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *loading;

@property (nonatomic) BOOL indicesFlag;

@property (nonatomic, retain) NSDictionary *marketIndicatorsDict;
@property (nonatomic, retain) NSDictionary *marketIndicesDict;

//Major Market Indicators
@property (nonatomic, retain) NSArray *exchanges;
@property (nonatomic, retain) NSDictionary *exchangeLabels;
@property (nonatomic, retain) NSDictionary *exchangeTickers;
@property (nonatomic, retain) NSMutableDictionary *tickerValues;
@property (nonatomic, retain) NSTimer *timerIndicators;


//Major Market Indices
@property (nonatomic, retain) NSDictionary *indicesNames;
@property (nonatomic, retain) NSArray *indicesSymbols;
@property (nonatomic, retain) NSMutableDictionary *indicesValues;

//NASDAQ Basic entitlement
@property (nonatomic, retain) NSString *nb;

@end

@implementation MajorIndicatorsIndicesVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _exchanges = [[NSArray alloc] initWithObjects:@"NYSE Only", @"NYSE Composite", @"AMEX", @"NASDAQ", @"DJIA", nil];
    
    _exchangeLabels = [[NSDictionary alloc] initWithObjectsAndKeys:
                       @[@"Up Volume", @"Down Volume", @"CUM Volume", @"Advancers", @"Decliners", @"Unchanged"], @"NYSE Only",
                       @[@"Up Volume", @"Down Volume", @"CUM Volume", @"Advancers", @"Decliners", @"Unchanged", @"Issues Up/Down Ratio (TICK)", @"Short Term Trade Index (TRIN)"], @"NYSE Composite",
                       @[@"Up Volume", @"Down Volume", @"CUM Volume", @"Advancers", @"Decliners", @"Unchanged"], @"AMEX",
                       @[@"Up Volume", @"Down Volume", @"CUM Volume", @"Advancers", @"Decliners", @"Unchanged", @"Issues Up/Down Ratio (TIKQ)", @"Short Term Trade Index (TRINQ)"], @"NASDAQ",
                       @[@"Issues Advance/Decline Ratio"], @"DJIA",
                       nil];
    
    _exchangeTickers = [[NSDictionary alloc] initWithObjectsAndKeys:
                        @[@".NUVOL", @".NDVOL", @".NTVOL", @".NADV", @".NDEC", @".NCHG"], @"NYSE Only",
                        @[@".UVOL", @".DVOL", @".VOL", @".ADV", @".DECL", @".UCHG", @".TICK", @".TRIN"], @"NYSE Composite",
                        @[@".UVOLA", @".DVOLA", @".VOLA", @".ADVA", @".DECLA", @".UCHGA"], @"AMEX",
                        @[@".UVOLQ", @".DVOLQ", @".VOLQ", @".ADVQ", @".DECLQ", @".UCHGQ", @".TICKQ", @".TRINQ"], @"NASDAQ",
                        @[@".TICKI"], @"DJIA",
                       nil];
    
    NSArray *namesArray = [[NSArray alloc] initWithObjects:
                           @"DJIA",
                           @"DJ Transport Avg.",
                           @"DJ Utilities Avg.",
                           @"NASDAQ Comp.",
                           @"NASDAQ 100",
                           @"S & P 500",
                           @"S & P 100",
                           @"NYSE Comp.",
                           @"Russell 2000",
                           @"Russell 1000",
                           @"Semi Index",
                           @"Internet Index",
                           @"Volatility Index",
                           @"Oil Service Index",
                           @"MS Hi-Tech Index",
                           @"30-year bond",
                           @"10-year note",
                           @"5-year note",
                           @"13-Week T-Bill",
                           @"Gold & Silver Index", nil];
    
    _indicesSymbols = [[NSArray alloc] initWithObjects:
                       @"I:DJI",
                       @"I:DJT",
                       @"I:DJU",
                       @"I:COMP",
                       @"I:NDX",
                       @"I:SPX",
                       @"I:OEX",
                       @"I:NYA",
                       @"I:RUT",
                       @"I:RUI",
                       @"I:SOX",
                       @"I:IIX",
                       @"I:VIX",
                       @"I:OSX",
                       @"I:MSH",
                       @"I:TYX",
                       @"I:TNX",
                       @"I:FVX",
                       @"I:IRX",
                       @"I:XAU", nil];
    
    _indicesNames = [[NSDictionary alloc] initWithObjects:namesArray forKeys:_indicesSymbols];
    
    _nb = isNASDAQBasicEntitled ? @"1" : @"0";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    //NSLog(@"%s",__PRETTY_FUNCTION__);
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return (toInterfaceOrientation==UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation==UIInterfaceOrientationLandscapeRight);
}

#pragma mark -
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    _titleItem.title = _viewTitle;

    _indicesFlag = [_viewTitle isEqualToString:@"Major Market Indices"];
    
    _tableView.tag = _indicesFlag ? INDICES_TABLE_TAG : INDICATORS_TABLE_TAG;
    [_tableView reloadData];
    
    _indicesFlag ? [Flurry logEvent:@"Major Market Indices"] : [Flurry logEvent:@"Major Market Indicators"];
    
    [self majorMarketData];
    _timerIndicators = [NSTimer scheduledTimerWithTimeInterval:30.0
                                                        target:self
                                                      selector:@selector(majorMarketData)
                                                      userInfo:nil
                                                       repeats:YES];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    [_timerIndicators invalidate];
}

- (IBAction)closeView
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark Getting Data
- (void)majorMarketData
{
    [_loading startAnimating];
    
    if (_indicesFlag) {
        [self performSelectorInBackground:@selector(fetchMajorMarketIndicesData) withObject:nil];
    }
    else
    {
        NSArray *combined = [[NSArray alloc] init];
        for (NSString *exchange in _exchanges) {
            combined = [combined arrayByAddingObjectsFromArray:_exchangeTickers[exchange]];
        }
        
        [self performSelectorInBackground:@selector(fetchMajorMarketIndicatorsFor:) withObject:combined];
    }
}

- (void)fetchMajorMarketIndicatorsFor:(NSArray*)array
{
    NSString *url = [NSString stringWithFormat:SNAP_QUOTE_URL, _nb, [array componentsJoinedByString:@","]];
    NSDictionary *json = [ProjectHandler dataFromAPIWithStringURL:url];
    
    if(!_tickerValues)
        _tickerValues = [[NSMutableDictionary alloc] init];
        
    NSNumberFormatter *numberFormatter=[[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    
    [numberFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [numberFormatter setCurrencySymbol:@""];
    
    
    for (NSString *exchg in _exchanges) {

        NSMutableArray *values = [[NSMutableArray alloc] init];

        for (NSString *key in _exchangeTickers[exchg]) {
            
            NSNumber *val = [NSNumber numberWithFloat:[json[key][@"last"] floatValue]];
            [values addObject:[numberFormatter stringFromNumber:val]];
        }
        
        [_tickerValues setValue:values forKey:exchg];
    }
    
    [self performSelectorOnMainThread:@selector(finishingAPICall) withObject:nil waitUntilDone:YES];
}

- (void)fetchMajorMarketIndicesData
{
    NSString *url = [NSString stringWithFormat:SNAP_QUOTE_URL, _nb, [_indicesSymbols componentsJoinedByString:@","]];
    NSDictionary *indicesJSON = [ProjectHandler dataFromAPIWithStringURL:url];
    
    if(!_indicesValues)
        _indicesValues = [[NSMutableDictionary alloc] init];
    
    NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
    numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
    numberFormatter.usesGroupingSeparator = YES;
    numberFormatter.groupingSeparator = @",";
    numberFormatter.groupingSize = 3;
    numberFormatter.maximumFractionDigits = 2;

    NSDictionary *exchangeHashDict = DICT_ExchangeHash;

    for (NSString *index in _indicesSymbols) {

        NSMutableArray *temp = [[NSMutableArray alloc] init];

        [temp addObject:_indicesNames[index]];
        [temp addObject:[NSString stringWithFormat:@"           %@",index]];

        //check entitlement
        BOOL isEntitled = NO;
        if ([exchangeHashDict[indicesJSON[index][@"exchangeId"]] count] && [exchangeHashDict[indicesJSON[index][@"exchangeId"]][kAuthorized] isEqualToString:kAuthorized])
        {
            isEntitled = YES;
        }

        if (indicesJSON[index] && isEntitled) {
            [temp addObject:indicesJSON[index][@"last"]];
            
            NSString *chg = indicesJSON[index][@"chg"];
            if (chg.floatValue>0.0) {
                [temp addObject:[NSString stringWithFormat:@"+%@",chg]];
                [temp addObject:[NSString stringWithFormat:@"+%@",indicesJSON[index][@"perChg"]]];
            }
            else {
                [temp addObject:chg];
                [temp addObject:indicesJSON[index][@"perChg"]];
            }
        }
        else
        {
            [temp addObject:@""];
            [temp addObject:@""];
            [temp addObject:@""];
        }
        
        [_indicesValues setValue:temp forKey:index];
    }
    
    [self performSelectorOnMainThread:@selector(finishingAPICall) withObject:nil waitUntilDone:YES];
}

- (void)finishingAPICall
{
    [_tableView reloadData];
    [_loading stopAnimating];
}

#pragma mark Custom Cell
- (UITableViewCell*)indicatorsCell:(UITableViewCell*)cell ForIndexPath:(NSIndexPath*)indexPath
{
    for (UILabel *subview in cell.contentView.subviews) {
        [subview removeFromSuperview];
    }
    
    CGFloat width = cell.frame.size.width-10;
    CGFloat height = cell.frame.size.height;
    
    UILabel *leftLabel  = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, width/2, height)];
    UILabel *rightLabel = [[UILabel alloc] initWithFrame:CGRectMake(width/2, 0, width/2, height)];
    
    leftLabel.text = _exchangeLabels[_exchanges[indexPath.section]][indexPath.row];
    rightLabel.text = _tickerValues[_exchanges[indexPath.section]][indexPath.row];
    
    leftLabel.backgroundColor = [UIColor clearColor];
    rightLabel.backgroundColor = [UIColor clearColor];
    
    leftLabel.textColor = [UIColor whiteColor];
    rightLabel.textColor = [UIColor whiteColor];
    
    leftLabel.font = [UIFont fontWithName:@"Helvetica" size:15];
    rightLabel.font = [UIFont fontWithName:@"Helvetica" size:15];
    
    [cell.contentView addSubview:leftLabel];
    [cell.contentView addSubview:rightLabel];
    
    cell.contentView.backgroundColor = [UIColor clearColor];

    return cell;
}

- (UITableViewCell*)indicesCell:(UITableViewCell*)cell ForIndexPath:(NSIndexPath*)indexPath
{
    for (UILabel *subview in cell.contentView.subviews) {
        [subview removeFromSuperview];
    }
    
    NSInteger numberOfColumns = 5;
    
    CGFloat width = (cell.frame.size.width-10)/numberOfColumns;
    CGFloat height = cell.frame.size.height;
    
    CGFloat x = 5;
    UILabel *label;
    
    for (int i=0; i<numberOfColumns; i++) {
        
        label = [[UILabel alloc] initWithFrame:CGRectMake(x, 0, width, height)];
        label.textColor = [UIColor whiteColor];
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont fontWithName:@"Helvetica" size:15];
        
        label.text = _indicesValues[_indicesSymbols[indexPath.row]][i];
        
        CGFloat changeVal = [_indicesValues[_indicesSymbols[indexPath.row]][3] floatValue];
        if (changeVal<0) {
            label.textColor = [UIColor redColor];
        }
        else if (changeVal>0) {
            label.textColor = [UIColor greenColor];
        }

        if (i>1) {
            label.textAlignment = NSTextAlignmentRight;
        }
        
        [cell.contentView addSubview:label];
        
        x += width;
    }
    
    return cell;
}

#pragma mark - UITableView Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return (tableView.tag == INDICATORS_TABLE_TAG) ? _exchanges.count : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rowsCount = (tableView.tag == INDICATORS_TABLE_TAG) ? [_exchangeLabels[_exchanges[section]] count] : _indicesValues.count;
    return rowsCount;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return INDICES_TABLE_ROWHEIGHT*0.8;
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (tableView.tag == INDICATORS_TABLE_TAG)
        return _exchanges[section];
    else return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView.tag == INDICES_TABLE_TAG)
    {
        NSArray *headers = [NSArray arrayWithObjects:@"Exchange/Index", @"Symbol", @"Last", @"Chg", @"%Chg", nil];
        
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, INDICES_TABLE_ROWHEIGHT*0.8)];
        headerView.backgroundColor = [UIColor lightGrayColor];
        
        CGFloat width = (headerView.frame.size.width-10)/headers.count;

        CGFloat x = 5;
        UILabel *label;
        
        for (int i=0; i<headers.count; i++)
        {
            label = [[UILabel alloc] initWithFrame:CGRectMake(x, 0, width, INDICES_TABLE_ROWHEIGHT*0.8)];
            
            label.textColor = [UIColor whiteColor];
            label.backgroundColor = [UIColor clearColor];
            label.font = [UIFont fontWithName:@"Helvetica-Bold" size:15];

            label.text = headers[i];
            
            label.textAlignment = i>1 ? NSTextAlignmentRight : NSTextAlignmentCenter;

            [headerView addSubview:label];
            
            x += width;
        }
        
        return headerView;
    }
    else return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == INDICATORS_TABLE_TAG)
        return 26.0;
    else
        return INDICES_TABLE_ROWHEIGHT;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        
        static NSString *CellIdentifier = @"Cell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, _tableView.frame.size.width, INDICES_TABLE_ROWHEIGHT);
        }

        UIImageView *bgImage = [[UIImageView alloc] init];
        bgImage.image = !(indexPath.row%2) ? [UIImage imageNamed:@"OddBg@2x.png"] : [UIImage imageNamed:@"EvenBg@2x.png"];
        cell.backgroundView = bgImage;
        
        if (tableView.tag == INDICATORS_TABLE_TAG) {
            return [self indicatorsCell:cell ForIndexPath:indexPath];
        }
        else return [self indicesCell:cell ForIndexPath:indexPath];
        
    }
    @catch (NSException *exception) {
        //NSLog(@"EXCEPTION CAUGHT in %s\n%@",__PRETTY_FUNCTION__,exception.description);
    }
}

#pragma mark UITableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
