//
//  OptionsPlusViewController.h
//  Quodd_iPad
//
//  Created by Nishant Anthwal on 2/21/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TickerTableHeader.h"

@interface OptionsPlusViewController : UIViewController <TickerTableHeaderDelegate, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSString *root;
@property (strong, nonatomic) NSArray *columnArray;

@property (strong, nonatomic) IBOutlet UIButton *menuToggleButton;

- (IBAction)backPressed;
- (IBAction)backgroundTouched;

- (IBAction)menuButtonPressed;

@end
