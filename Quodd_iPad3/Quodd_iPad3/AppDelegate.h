//
//  AppDelegate.h
//  Quodd_iPad2
//
//  Created by Nishant on 25/06/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LoginViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) LoginViewController *loginVC;
@property (strong, nonatomic) NSDictionary *columnWidth;

@end
