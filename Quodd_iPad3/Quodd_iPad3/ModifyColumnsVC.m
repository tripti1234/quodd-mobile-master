//
//  ModifyColumnsVC.m
//  Quodd_iPad2
//
//  Created by Nishant on 27/06/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import "ModifyColumnsVC.h"

#define ALL_COLUMNS_TABLE       1
#define SELECTED_COLUMNS_TABLE  2

#define TICKER_TABLE_WIDTH 754.0

@interface ModifyColumnsVC ()

@property (nonatomic, retain) NSArray *allColumns;
@property (nonatomic, retain) NSMutableArray *availableColumns;

@end

@implementation ModifyColumnsVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = VIEW_BACKGROUND_IMAGE;
    _allColumns = [[NSArray alloc] initWithArray:_formattedKeys.allKeys];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    _allColumns = [_allColumns sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];

    _availableColumns = [[NSMutableArray alloc] initWithArray:_allColumns];
    [_availableColumns removeObjectsInArray:_selectedColumns];

    [_selectedColumns removeObjectAtIndex:0]; //remove ticker name
    
    
    [_tableAllColumns reloadData];
    [_tableSelectedColumns reloadData];
    
    [_tableSelectedColumns setEditing:YES
                             animated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return (UIInterfaceOrientationIsLandscape(toInterfaceOrientation));
}

#pragma mark -

- (IBAction)closeView
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)done
{
    static NSDictionary *columnWidth;
    
    if (!columnWidth) {
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:[(AppDelegate *)[[UIApplication sharedApplication] delegate] columnWidth]];
        
        [dict setValue:@"40" forKey:@"askExchg"];
        [dict setValue:@"40" forKey:@"bidExchg"];
        [dict setValue:@"40" forKey:@"marketCenter"];
        
        columnWidth = [[NSDictionary alloc] initWithDictionary:dict];
    }
    
    CGFloat buffer = 10;
    CGFloat desiredTotalWidth = TICKER_TABLE_WIDTH - 2*buffer;
   
    CGFloat expectedTotalWidth = 0.0;
    for (int i=0; i<_selectedColumns.count; i++) {
        expectedTotalWidth += [columnWidth[_selectedColumns[i]] floatValue];
    }
    
    CGFloat multiplicationFactor = desiredTotalWidth / expectedTotalWidth;

    if (multiplicationFactor > 1.0)     //factor increase to accomadate more space
    {
        [self dismissViewControllerAnimated:YES completion:^{
            
            [_selectedColumns insertObject:@"tickerNameByUser" atIndex:0];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kReloadDataInTable
                                                                object:nil
                                                              userInfo:@{@"newColumns": _selectedColumns}];
        }];
    }
    else
        [[[UIAlertView alloc] initWithTitle:@"Column Limit Reached"
                                    message:@"The columns you have chosen, do not fit into the ticker table."
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    
}

#pragma mark - UITableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return (tableView.tag == ALL_COLUMNS_TABLE) ? _availableColumns.count : _selectedColumns.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return (tableView.tag == ALL_COLUMNS_TABLE) ? @"Available Columns" : @"Selected Columns";
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
        
    cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fundaBg.png"]];
    cell.selectedBackgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fundaBg-active"]];

    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];

    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    if (tableView.tag == ALL_COLUMNS_TABLE)
    {
        cell.textLabel.text = _formattedKeys[_availableColumns[indexPath.row]];
    }
    else cell.textLabel.text = _formattedKeys[_selectedColumns[indexPath.row]];

    return cell;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleNone;
}

- (BOOL)tableView:(UITableView *)tableview shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
{
    NSString *stringToMove = _selectedColumns[sourceIndexPath.row];
    [_selectedColumns removeObjectAtIndex:sourceIndexPath.row];
    [_selectedColumns insertObject:stringToMove atIndex:destinationIndexPath.row];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try
    {
        [_tableAllColumns beginUpdates];
        [_tableSelectedColumns beginUpdates];
        
        if (tableView.tag == ALL_COLUMNS_TABLE)
        {
            NSString *switchObject = _availableColumns[indexPath.row];
            
            [_tableAllColumns deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationRight];
            [_tableSelectedColumns insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:_selectedColumns.count inSection:0]] withRowAnimation:UITableViewRowAnimationLeft];
            
            [_availableColumns removeObject:switchObject];
            [_selectedColumns addObject:switchObject];
        }
        else
        {
            NSString *switchObject = _selectedColumns[indexPath.row];
            
            [_tableSelectedColumns deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
            [_tableAllColumns insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationRight];
            
            [_selectedColumns removeObject:switchObject];
            [_availableColumns insertObject:switchObject atIndex:0];
        }
        
        [_tableAllColumns endUpdates];
        [_tableSelectedColumns endUpdates];
    }
    @catch (NSException *e)
    {
        //NSLog(@"%@",e.description);
    }
}

@end