//
//  CompanyFundamentalsViewController.h
//  Quodd_iPad
//
//  Created by Nishant on 10/05/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CompanyFundamentalsViewController : UIViewController <UIWebViewDelegate, UITableViewDataSource, UITableViewDelegate, UIPopoverControllerDelegate, UISearchBarDelegate, UISearchDisplayDelegate>
{
    IBOutlet UITableView *fundamentalsList;
    IBOutlet UIWebView *companyFundaWebView;
    IBOutlet UIActivityIndicatorView *webviewLoadingIndicator;
}

@property (nonatomic,retain) NSString *tickerSymbol;
@property (nonatomic,retain) NSString *selectedTitle;
@property (nonatomic,retain) NSArray *companyFundaTitles;

- (IBAction)backPressed;
- (IBAction)backgroundTouched;

@end
