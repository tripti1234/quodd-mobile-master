//
//  MiscViewController.m
//  Quodd_iPad
//
//  Created by Nishant on 16/05/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import "MiscViewController.h"

@interface MiscViewController ()

@property (nonatomic,retain) NSArray *miscURLs;

@end

@implementation MiscViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    miscTable.backgroundColor = VIEW_BACKGROUND_IMAGE;

    _miscURLs = [[NSArray alloc] initWithObjects:
                 TREASURY_PAGE_URL,
                 SPLITS_AND_BUYBACKS_URL,
                 MOVERS_AND_SHAKERS_URL,
                 HISTORICAL_PRICES_URL,
                 STOCK_SCREENER_URL,
                 MUTUAL_FUND_SCREENER_URL,
                 EARNINGS_CALENDAR_URL,
                 [NSString stringWithFormat:ECONOMIC_CALENDAR_PLUS_URL,currentUserName], nil];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return (toInterfaceOrientation==UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation==UIInterfaceOrientationLandscapeRight);
}

#pragma mark -
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [Flurry logEvent:@"Misc View"];
    [self loadMiscViewForIndex:_miscIndex];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    [miscWebView loadHTMLString:@"<html></html>" baseURL:nil];
}

- (IBAction)closeView
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)loadMiscViewForIndex:(NSInteger)index
{
    [miscWebView loadHTMLString:@"<html></html>" baseURL:nil];

    miscTitle.title = _miscTitles[index];
    
    NSURL *newsURL = [NSURL URLWithString:_miscURLs[index]];
    [miscWebView loadRequest:[NSURLRequest requestWithURL:newsURL]];
    
    NSIndexPath *selectedIndexPath = [NSIndexPath indexPathForRow:index inSection:0];
    [miscTable reloadRowsAtIndexPaths:@[selectedIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];

    [miscTable selectRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] animated:YES scrollPosition:UITableViewScrollPositionBottom];
}

#pragma mark - UIWebViewDelegate
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    webView.hidden = YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    webView.hidden = NO;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    webView.hidden = NO;
}

#pragma mark - UITableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _miscTitles.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30.0;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *headerLabel = [[UILabel alloc] init];
    headerLabel.text = @"  Miscellaneous";
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.textColor = [UIColor whiteColor];
    headerLabel.font = [UIFont boldSystemFontOfSize:20.0];
    
    return headerLabel;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    cell.selectedBackgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fundaBg-active"]];
    cell.backgroundView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"fundaBg.png"]];

    cell.textLabel.font = [UIFont boldSystemFontOfSize:14];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    cell.textLabel.text = _miscTitles[indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self loadMiscViewForIndex:indexPath.row];
}

@end
