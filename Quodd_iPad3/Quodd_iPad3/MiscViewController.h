//
//  MiscViewController.h
//  Quodd_iPad
//
//  Created by Nishant on 16/05/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MiscViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UIWebView *miscWebView;
    IBOutlet UIBarButtonItem *miscTitle;
    
    IBOutlet UITableView *miscTable;
}

@property(nonatomic)NSInteger miscIndex;
@property (nonatomic,retain) NSArray *miscTitles;

- (IBAction)closeView;

@end
