//
//  Option.h
//  Quodd_iPad2
//
//  Created by Paxcel Technologies on 15/07/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Ticker.h"

@interface Option : NSObject

@property (assign, nonatomic) double expireTime;
@property (assign, nonatomic) double volatility;
@property (assign, nonatomic) double interestRate;

@property (strong, nonatomic) NSArray *optionItemArray;

- (id) initWithDictionary: (NSDictionary *) dictionary
               volatility: (NSString *) volatility;

- (void) updateOptionItemArrayWithDictionary: (NSDictionary *) dictionary
                                  rootTicker: (Ticker *) rootTicker;

@end
