//
//  NALabelAnimation.m
//  Quood_iPad
//
//  Created by Nishant on 13/06/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import "NALabelAnimation.h"

@interface NALabelAnimation ()

@property (nonatomic, strong) UILabel *label;
@property (nonatomic, strong) NSArray *tickers;
@property (nonatomic, strong) NSDictionary *dataDict;
@property (nonatomic) NSInteger count;

@property (nonatomic) CGRect initialFrame;
@property (nonatomic) CGRect centerFrame;
@property (nonatomic) CGRect finalFrame;

@end

@implementation NALabelAnimation

- (id)initWithFrame:(CGRect)frame andTickerArray:(NSArray*)array
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        CGFloat buffer = 5;
        
        _centerFrame = CGRectMake(frame.origin.x + buffer,
                                  frame.origin.y,
                                  frame.size.width - 2*buffer,
                                  frame.size.height);
        
        _initialFrame = CGRectMake(_centerFrame.origin.x,
                                   _centerFrame.origin.y + _centerFrame.size.height,
                                   _centerFrame.size.width,
                                   _centerFrame.size.height);
        
        _finalFrame = CGRectMake(_centerFrame.origin.x,
                                 _centerFrame.origin.y - _centerFrame.size.height,
                                 _centerFrame.size.width,
                                 _centerFrame.size.height);
       
        
        _label = [[UILabel alloc] initWithFrame:_initialFrame];
        _label.textAlignment = NSTextAlignmentCenter;
        _label.font = [UIFont fontWithName:@"Helvetica Neue-Bold" size:15];
        _label.textColor = [UIColor whiteColor];
        _label.backgroundColor = [UIColor clearColor];
        [_label setAdjustsFontSizeToFitWidth:YES];
        [self addSubview:_label];
        
        self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"fundaBg-active.png"]];

        _tickers = [[NSArray alloc] initWithArray:array];
        _count = 0;
        
        [self getDataForTickers];
    }
    
    return self;
}

#pragma mark -
- (void)getDataForTickers
{
    NSString *snapQuoteURL = [NSString stringWithFormat:SNAP_QUOTE_URL, isNASDAQBasicEntitled ? @"1" : @"0", [_tickers componentsJoinedByString:@","]];
    _dataDict = [[NSDictionary alloc] initWithDictionary:[ProjectHandler dataFromAPIWithStringURL:snapQuoteURL]];
    
    [self performSelectorOnMainThread:@selector(startAnimation) withObject:nil waitUntilDone:NO];
}

- (void)startAnimation
{
    _label.frame = _initialFrame;
    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         
                         if (_dataDict.count>0) {
                             
                             NSString *currentMktIndex = _dataDict.allKeys[_count];
                             
                             NSString *name = _dataDict[currentMktIndex][@"ticker"];
                             NSString *last = _dataDict[currentMktIndex][@"last"];
                             NSString *change = _dataDict[currentMktIndex][@"chg"];
                             
                             if (change.floatValue>0) {
                                 change = [NSString stringWithFormat:@"↑%@",change];
                                 _label.textColor = [UIColor greenColor];
                             }
                             else if (change.floatValue<0) {
                                 change = [NSString stringWithFormat:@"↓%@",change];
                                 _label.textColor = [UIColor redColor];
                             }
                             
                             _label.text = [NSString stringWithFormat:@"%@   %@   %@",name,last,change];
                         }
                         else _label.text = @"US Markets are closed.";
                         
                         _label.frame = _centerFrame;
                         
                         _count = (_count+1==_dataDict.count) ? 0 : _count+1;
                         
                     }
                     completion:^(BOOL finished) {
                         
                         [self finishAnimation];
                     }];
}

- (void)finishAnimation
{
    [UIView animateWithDuration:0.5
                          delay:1.5
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         _label.frame = _finalFrame;
                     }
                     completion:^(BOOL finished) {
                         [self startAnimation];
                     }];
}

@end
