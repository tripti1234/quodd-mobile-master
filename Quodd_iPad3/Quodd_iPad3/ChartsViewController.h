//
//  ChartsViewController.h
//  Quodd_iPad3
//
//  Created by Nishant on 31/07/13.
//  Copyright (c) 2013 Paxcel Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChartsViewController : UIViewController <UITextFieldDelegate, UIAlertViewDelegate>

@property (nonatomic, retain) IBOutlet UITextField *tickerID;
@property (nonatomic, retain) IBOutlet UISegmentedControl *timeSpanSegment;
@property (nonatomic, retain) IBOutlet UISegmentedControl *chartTypeSegment;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *indicator;
@property (nonatomic, retain) IBOutlet UIImageView *chartImage;

@property (nonatomic, retain) NSString *tickerName;

- (IBAction)close;
- (IBAction)segmentChanged:(UISegmentedControl*)senderSegment;
- (IBAction)addToCompare;
- (IBAction)resetCompare;

@end
