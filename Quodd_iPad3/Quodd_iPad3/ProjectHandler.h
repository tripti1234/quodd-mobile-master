//
//  ProjectHandler.h
//  Quodd_iPad2
//
//  Created by Nishant on 25/06/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CustomAlertView.h"

@interface ProjectHandler : NSObject

@property (nonatomic, retain) NSMutableArray *subscribedSymbols;
@property (nonatomic, retain) NSMutableArray *pendingSymbols;
@property (nonatomic, retain) NSMutableArray *nonSubscribedSymbols;


+ (id)sharedHandler;

+ (id)dataFromAPIWithStringURL:(NSString *)urlString;
+ (NSMutableURLRequest*)urlRequestWithURL:(NSString*)urlString;

+ (void)addDictionary:(NSDictionary*)dictionary toPListWithName:(NSString*)plistName;
+ (NSDictionary*)getDictionaryFromPList:(NSString*)plistName;

+ (CustomAlertView*)showHUDWithTitle:(NSString *)text;
+ (void)hideHUD:(CustomAlertView*)alert;

@end
